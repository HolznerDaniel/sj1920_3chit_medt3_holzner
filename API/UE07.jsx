﻿/*  UE07.4
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers;                             //Alle Ebenen des Dokuments  
$.writeln(layer.length);

*/

/* UE07.5
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers;                             //Alle Ebenen des Dokuments  
$.writeln ("Layer 1 is " + layer[0]);               //String + stelle der Ebene im array
$.writeln ("Layer 2 is " + layer[1]);
$.writeln ("Layer 3 is " + layer[2]);
$.writeln ("Layer 4 is " + layer[3]);

*/

/* UE07.6
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers;                             //Alle Ebenen des Dokuments  
layer.add ();                                                //Fügt am Anfang (Stelle 0) eine Ebene hinzu   
layer[0].name = "Demo Layer 4";           //Verändert den Namen des Layers, an der Stelle 0 

*/

/*  UE07.7
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers;                             //Alle Ebenen des Dokuments  
if(layer[3].textFrames != null)                 //Stelle 3, da wir vorhin eine Ebene einfügt haben.
{
$.writeln(layer[3] + " Item: Textframe is included");
}

*/

/* UE07.8
    
var doc = app.activeDocument;                       //Im Activen Dokument
var layer = doc.layers;                                     //Alle Ebenen des Dokuments  
$.writeln(layer[3].textFrames[0].contents);     //Der Content des TextFrames an der Stelle 0 wird ausgelesen (Textframes sind in einem Array gespeichtert)

*/

/* UE07.9  Lösungsvariante 1

var doc = app.activeDocument;                     //Im Activen Dokument
var layer = doc.layers;                              //Alle Ebenen des Dokuments  
var Items = layer[3].pageItems;                  //Items sind alle Elemente im dem Layer der Stelle 3
Items[0].contents = "Mein Name ist Jeff";       //Der Content des Items and der Stelle 0 wird verändert
$.writeln(Items[0].contents);                        

*/

/* UE07.9  Lösungsvariante 2
  
var doc = app.activeDocument;                                //Im Activen Dokument
var layer = doc.layers;                                                 //Alle Ebenen des Dokuments  
var TextFrameText = layer[3].textFrames[0];           //ACHTUNG!!! TextFrame funktioniert nicht als Variablenname! Im Layer der Stelle 3 wird das erste Textframe genommen
TextFrameText.contents = TextFrameText.contents.replace ("Jeff", "Anton Haulimamauli");             //Der Content des Textframes wird replaced.
$.writeln(TextFrameText.contents);

*/

/* UE07.10
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers;                             //Alle Ebenen des Dokuments  
var text = layer[3].textFrames[0];
text.position = [100, -50];

*/

/* UE07.11
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers[0];                             //Alle Ebenen des Dokuments  
layer.textFrames.add();
layer.textFrames.add();
layer.textFrames.add();
var textframes = layer.textFrames;
textframes[0].contents = "Herbert";
textframes[1].contents = "Heinz";
textframes[2].contents = "Gilbert";
textframes[0].position = [50, -50];
textframes[1].position = [175, -50];
textframes[2].position = [300, -50];

*/

/* UE07.12
    
var doc = app.activeDocument;               //Im Activen Dokument
var promt = prompt ("Bitte gewünschten Namen eingeben", "", "Namenseingabe");            //prompt (prompt, default, title) => 1. Der Text der im Feld stehen soll. 2. Was im Eingabefeld schon im vorhinein stehen soll. 3. Der Titel des Feldes.
$.writeln (promt);                          //Die Variable sollte nicht "prompt" genannt werden, da dies sonst den prompt-Command annimmt.

*/

/* UE07.13
    
var doc = app.activeDocument;               //Im Activen Dokument
var layer = doc.layers["Text"];                 //In der Ebene mit dem Namen "Text"
var text = layer.textFrames[1].contents;
$.writeln (text);

*/

/* UE07.14
    
var doc = app.activeDocument;               //Im Activen Dokument
var nametextframe = doc.layers["Text"].textFrames[1];
var promt = prompt ("Bitte gewünschten Namen eingeben", "", "Namenseingabe");
nametextframe.contents = promt;

*/

/* UE07.15
    
var doc = app.activeDocument;               //Im Activen Dokument
var layerbadge = doc.layers["Badge"];
var layertext = doc.layers["Text"];
var date = new Date();
var badgecontent =  layerbadge.textFrames[0];
var currentyear = badgecontent.contents.replace(badgecontent.contents.substr (9), date.getFullYear());               //subst(start, length) => startet ab inclusive dem 9. Buchstaben und geht bis zum Ende.
badgecontent.contents = currentyear;
var textcontent = layertext.textFrames[2];
var currenttext = textcontent.contents.replace(textcontent.contents.substr(textcontent.contents.indexOf(".") + 2), date.getDate() + "." + (date.getMonth() + 1 + "." + date.getFullYear()));               //IndexOf gibt den Index zurück, an welcher stelle der gesuchte string steht. getMonth zählt ab 0.
textcontent.contents = currenttext;                 //WICHTIG! Um den Content eines TextFrames zu ändern benötigt man den ganzen TextFrame und nicht nur den Content des TextFrames.

*/

/* UE07.16
    
var doc = app.activeDocument;               //Im Activen Dokument
var filename = new File("Certificate-Student-UE7.1.pdf");
var saveoptions = new PDFSaveOptions();
saveoptions.requireDocumentPassword = true;
saveoptions.documentPassword = "Yes,weCan";
doc.saveAs(filename, saveoptions);

*/





